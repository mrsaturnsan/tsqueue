#ifndef TSQUEUE_HPP
#define TSQUEUE_HPP

#include <memory>
#include <mutex>
#include <vector>
#include <functional>
#include <iterator>

namespace SaturnLib
{
    class TSQueue
    {
    private:
        class ITask
        {
        public:
            virtual ~ITask() = default;
            virtual void execute() const = 0;
        };

        template <typename T>
        class Task : public ITask
        {
        private:
            T m_callable;
        public:
            Task(T&& callable) : m_callable{std::move(callable)}
            {

            }

            void execute() const override
            {
                m_callable();
            }
        };
        
        std::mutex m_mutex;
        std::vector<std::unique_ptr<ITask>> m_tasks;
    public:
        template <typename T, typename... Args>
        void add_task(T&& callable, Args&&... args)
        {
            std::unique_ptr<ITask> task{new Task{[=] {
                std::invoke(callable, std::forward<Args>(args)...);
            }}};
            
            std::lock_guard<std::mutex> guard{m_mutex};

            m_tasks.emplace_back(std::move(task));
        }

        void run_task(size_t task_count)
        {
            if (task_count == 0)
                return;

            std::vector<std::unique_ptr<ITask>> t_tasks;

            {
                std::lock_guard<std::mutex> guard{m_mutex};

                if (m_tasks.empty())
                    return;
                else if (task_count > m_tasks.size())
                    task_count = m_tasks.size();

                t_tasks.reserve(task_count);

                auto end = m_tasks.begin() + task_count;
                t_tasks.insert(t_tasks.begin(), std::make_move_iterator(m_tasks.begin()), 
                                                std::make_move_iterator(end));
                m_tasks.erase(m_tasks.begin(), end);
            }
            
            for (const auto& i : t_tasks)
                i->execute();
        }
    };
}

#endif
