#include <iostream>
#include <thread>

#include "tsqueue.hpp"

int main()
{
    SaturnLib::TSQueue queue;

    queue.add_task([] {
        std::cout << "Task 1\n";
    });

    queue.add_task([] {
        std::cout << "Task 2\n";
    });

    queue.add_task([] {
        std::cout << "Task 3\n";
    });

    auto a = std::thread(&SaturnLib::TSQueue::run_task, &queue, 3);
    auto b = std::thread(&SaturnLib::TSQueue::run_task, &queue, 3);
    auto c = std::thread(&SaturnLib::TSQueue::run_task, &queue, 3);

    a.join();
    b.join();
    c.join();
    
    return 0;
}
