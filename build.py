import argparse
import subprocess

parser = argparse.ArgumentParser(description="Utility file for CMake builds.")

parser.add_argument('-b', '--build', default="Release",
                    help='Specify either debug or release mode.')  

args = parser.parse_args()

build_directory = "build"

cmakeCmd = ["cmake.exe", "-H.", "-B", build_directory]
cmakeBuildCmd = ["cmake.exe", "--build", build_directory, "--config", args.build]

subprocess.call(cmakeCmd)
subprocess.call(cmakeBuildCmd)